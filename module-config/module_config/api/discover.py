
import os
import json
import flask
import inspect

from glob import glob

from flask import request
from flask_restful import Resource

from importlib import import_module

from .discover_exceptions import KEEP_IMPORTED_ELEMENTS, EXCLUDED_CLASS, EXCLUDED_FILES, EXCLUDED_VARIABLES


def construct_dic_file(file, top_module, keep_imp_elements=KEEP_IMPORTED_ELEMENTS):
    '''Return a dict describing the structure of the file - variables, functions and classes.'''

    lis_class = {}      # list of the classes contained in the file
    lis_function = {}   # list of the functions contained in the file
    lis_variable = {}   # list of the variables contained in the file
    for (name, member) in inspect.getmembers(file):
        
        if not(name.startswith('__') and name.endswith('__')) and name not in EXCLUDED_VARIABLES:

            if member.__class__ in EXCLUDED_CLASS:
                print('== Class == ')
                print(member)
                print('== Excluded == ')
                continue
            if inspect.isclass(member):
                lis_class[name] = member
            if inspect.isfunction(member):
                lis_function[name] = member
            if not inspect.ismodule(member):
                lis_variable[name] = member

    dic_file = {}
    for name, value in lis_variable.items():
        dic_file[name] = value 
        
    for class_name, class_obj in lis_class.items():
        dic_class = {}

        if keep_imp_elements or class_obj.__module__.startswith(top_module):
            for name, member in inspect.getmembers(class_obj):
                if inspect.isfunction(member):
                    sig = inspect.signature(member)
                    list_of_args = str(sig)[1:-1].replace(' ', '').split(',')

                    if 'self' in list_of_args:
                        list_of_args.remove('self')

                    dic_class[name] = list_of_args
            dic_file[class_name] = dic_class

    for function_name, function in lis_function.items():

        if keep_imp_elements or function.__module__.startswith(top_module):
            sig = inspect.signature(function)
            list_of_args = str(sig)[1:-1].replace(' ', '').split(',')
            dic_file[function_name] = list_of_args

    return dic_file


def construct_dic_outer_module():
    '''Return a dict describing the structure of the whole module.'''

    paths = glob('*/*/*.py')
    dic_outer_module = {}
    for path in paths:
        if os.sep + 'api' + os.sep not in path:   # we don't want to include the api module in the structure
            file_path = path.replace(os.sep, '.')
            top_module, inner_module, file_name, _ = file_path.split('.')

            if file_name in EXCLUDED_FILES:
                print('== File == ')
                print(file_name)
                print('== Excluded == ')
                continue

            if inner_module not in dic_outer_module:
                dic_outer_module[inner_module] = {}
            dic_inner_module = dic_outer_module[inner_module]
            file = import_module(file_path[:-3])
            dic_file = construct_dic_file(file, top_module, keep_imp_elements=KEEP_IMPORTED_ELEMENTS)
            if len(dic_file) > 0:   # if the file is not empty
                dic_inner_module[file_name] = dic_file

    dic_outer_module['_module_name'] = top_module

    return dic_outer_module


class DiscoverApi(Resource):
    '''
    Return a JSON containing the structure of the submodules
       - Only functions are taken into account
    
    If the JSON is not available, it is built.
    If you update manually the JSON, you have to respect the following rules:
        - no space before and after the = sign
        - if **kwargs is present, must be the last element of its list
    '''

    def get(self):
        here = os.path.dirname(os.path.abspath(__file__))
        json_path = os.path.join(os.path.dirname(here), 'structure.json')

        if os.path.exists(json_path):
            with open(json_path) as json_data:
                d = json.load(json_data)
            return d, 200
        
        else:
            # we construct the json.
            d = construct_dic_outer_module()
            s = json.dumps(d, indent=2)
            print('Start JSON')
            with open(json_path, 'w') as f:
                f.write(s)
            print('END Json')
            return d, 200
