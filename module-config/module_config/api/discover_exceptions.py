
from logging import Logger
from pymongo import MongoClient

EXCLUDED_VARIABLES = ['i_am_excluded']

EXCLUDED_CLASS = [Logger,
                  MongoClient]

EXCLUDED_FILES = ['log']

KEEP_IMPORTED_ELEMENTS = False
