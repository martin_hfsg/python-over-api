import sys
import requests
import pickle
import copy

import datetime as dt

# TODO: Modify __repr__ to indicate that this is not dict but API instance
# This class maps a dictionary into a simple Python object
class Dict2Obj(object):
    '''
    Class to transform a dict into an obj.
    
    Dict keys must be string which becomes the obj attributes' names.
    '''
    
    def __init__(self, dic):
        for key in dic:
            setattr(self, key, dic[key])
        else:
            return None
    def __repr__(self):
        return str(self.__dict__)
    def __copy__(self):
        return Dict2Obj(self.__dict__)


def generic_function(url_function, module_name, submodule_name, file_name, function_name,
                     method_name=None, **args):
    '''
    Post a request to the server asking to execute
    the corresponding remote function.
    
    :param str submodule_name: the name of the submodule
        which contains the function to be executed

    :param str file_name: the name of the file
        which contains the function to be executed

    :param str function_name: the name of the function
        to be executed

    :param str method_name: if this parameter is not None
        it means function_name is actually the name of a class ;
        thus method_name contains the name of the method of the class
        to be executed.

    :param dict **args: is a dict of arguments for the function
        we want to execute. Besides these arguments, it may also
    contains:
        - a variable _dic, which contains the object's __dict__
            and which is passed through the API
        - a variable _obj, which contains the object itself.
            It cannot be passed through the API, but it is used to
            locally modify the object after the result has been
            returned by the API.

    :return: result from the remote function.

    :raise: Exception if the POST request returns an error.
    '''

    args = dict((k, v) for (k, v) in args.items() if v != '__$__')
    
    # for special methods (such as __repr__), the obj is passed in _dic,
    # so we need to pop it out and replace it by its __dict__
    if method_name is not None and method_name[:2] == '__':
        obj = args['_dic']
        args['_dic'] = dict((k, v) for (k, v) in args['_dic'].__dict__.items()
                            if not hasattr(v, "__call__"))
    else:    
        obj = args.pop("_obj") if "_obj" in args else None

    dic = {'module': module_name + '.' + submodule_name + '.' + file_name,
           'function':function_name, 'args':args}

    if method_name:
        dic['method'] = method_name 
    req = requests.post(url_function, data=pickle.dumps(dic))
    incoming = pickle.loads(req.content)

    if req.status_code == 400 or req.status_code == 521:
        print(req.json()['error'])
        raise Exception(req.json()['error'])
    
    if req.status_code == 201:
        # populate instance with its __dict__
        instance = Dict2Obj(incoming['_dic'])
        methods = incoming['methods']
        # populate instance with its methods
        for method_name, list_of_args in methods.items():
            # we don't want to have access to the __init__ here.
            if method_name != '__init__':
                list_of_args.insert(0, "_dic")
                function = dic_to_func(url_function, module_name, submodule_name, file_name, function_name,
                                       list_of_args, method_name=method_name)
                setattr(instance, method_name, function)
        return change_instance_functions_to_pass_obj_and_dic(instance,
                                                             function_name)
    elif req.status_code == 202:

        method_result = incoming['method_result']
        # updating the instance __dict__
        obj.__dict__.update(incoming['_dic'])
        return method_result
    
    else:        
        return incoming


def dic_to_func(url_function, module_name, submodule_name, file_name, function_name, list_of_args,
                method_name=None):
    '''String manipulations to create lambda function from its signature.'''
    
    s = 'function = lambda '
    for attr in [x for x in list_of_args if x != '**kwargs']:
        s += attr + ','
    s += '**kwargs: generic_function("' + '", "'.join([url_function,
                                                       module_name,
                                                       submodule_name,
                                                       file_name,
                                                       function_name])
    if method_name:
        s += '", "' + method_name
    s += '", **{'
    for attr_name in [x.split('=')[0] for x in list_of_args if x != '**kwargs']:
        s += '"' + attr_name + '":' + attr_name + ','
    s = s[:-1] + "}, **kwargs)"
    print(s)
    exec(s)
    return locals()['function']


def change_instance_functions_to_pass_obj_and_dic(instance, function_name):
    '''String manipulations to pass the _obj and the _dic arguments
    to the lambda function without the user having to worry about it.'''

    global _dict_of_variables
    try:
        _dict_of_variables
    except:
        _dict_of_variables = {}
  #  if "_dict_of_variables" not in globals():
  #      globals()['_dict_of_variables'] = {}
    
    copy_of_instance = copy.copy(instance)
    
    tag = str(dt.datetime.now())
    _dict_of_variables[tag + '-0'] = instance
    _dict_of_variables[tag + '-1'] = copy_of_instance
    counter = 1
    
    dic_special_method = {}
    
    for name, func in instance.__dict__.items():
        if hasattr(func, '__call__'):
            counter += 1
            _dict_of_variables[tag + '-' + str(counter)] = name
            
            s = 'instance.temp = lambda '
            for attr in [x for x in list(func.__code__.co_varnames)
                         if x not in ['_dic', 'kwargs']]:
                s += attr + '="__$__",'
            s += '**kwargs: getattr(_dict_of_variables["' + tag + \
                 '-0"], _dict_of_variables["' + tag + '-' + str(counter) + '"])'
            s += '(_obj = _dict_of_variables["' + tag + \
                 '-1"], _dic=dict((k, v) for (k, v) in _dict_of_variables["' + \
                 tag + '-1"].__dict__.items() if not hasattr(v, "__call__")),**{'
            for attr_name in [x.split('=')[0] for x in list(func.__code__.co_varnames)
                              if x not in ['_dic', 'kwargs']]:
                s += '"' + attr_name + '":' + attr_name + ','
            if s[-1] == ',':
                s = s[:-1]
            s += '}, **kwargs)'
            print(s)
            exec(s)
            if name[:2] != '__':
                setattr(copy_of_instance, name, instance.temp)
            else:
                # special methods such as __repr__ are stacked
                # in dic_special_method, which is passed
                # to the class and not to the instance
                dic_special_method[name] = func
                del copy_of_instance.__dict__[name]
            delattr(instance, 'temp')
            
    copy_of_instance.__class__ = type(function_name, (Dict2Obj,), dic_special_method)
    return copy_of_instance


def integrate_in_sysmodules(module_name, remote_obj):

    sys.modules[module_name] = remote_obj

    for k1, v1 in remote_obj.__dict__.items():
    
        if isinstance(v1, Dict2Obj):
            remote_submodule = k1
            obj = v1
            
            sys.modules['.'.join([module_name, remote_submodule])] = obj
            
            for k2, v2 in obj.__dict__.items():
                remote_submodule2 = k2
                obj2 = v2
                
                if isinstance(obj2, Dict2Obj):
                    
                    sys.modules['.'.join([module_name, remote_submodule, remote_submodule2])] = obj2
    

def create_class_from_url(url, submodule_names=None, activate_local_import=True):
    '''Instanciate and return an object that reproduces
    the structure of a remote submodule.'''

    url_function = url + 'api/function'
    url_discover = url + 'api/discover'

    r = requests.get(url_discover)
    full_dic_files = r.json()

    module_name = full_dic_files['_module_name']

    dic_obj = {}
    dic_submodules = {}
    dic_remote_info = {}

    for submodule_name, dic_files in full_dic_files.items():

        if '_' in submodule_name and isinstance(dic_files, str):
            dic_remote_info[submodule_name] = dic_files
            continue

        if not submodule_names or submodule_name in submodule_names:
            for file_name, dic_function_names in dic_files.items():
                for function_name, list_of_args in dic_function_names.items():
                    if type(list_of_args) == dict:
                        list_of_args = list_of_args["__init__"]
                    if isinstance(list_of_args, list):
                        dic_files[file_name][function_name] = dic_to_func(url_function,
                                                                          module_name,
                                                                          submodule_name,
                                                                          file_name,
                                                                          function_name,
                                                                          list_of_args)
                    else:
                        dic_files[file_name][function_name] = list_of_args
            dic_submodules[submodule_name] = dic_files  
            
        obj = Dict2Obj(dic_files)
        for k, v in obj.__dict__.items():
            setattr(obj, k, Dict2Obj(v))

        dic_obj[submodule_name] = obj

    final_obj = Dict2Obj(dic_obj)
    for k, v in final_obj.__dict__.items():
        setattr(final_obj, k, v)

    for k, v in dic_remote_info.items():
        setattr(final_obj, k, v)

    if activate_local_import:
        integrate_in_sysmodules(module_name, final_obj)

    return final_obj


def create_class_from_name(module_name, submodule_names=None, activate_local_import=True):

    # if activate_local_import is True on module_config
    from module_config.config import url
    # TODO: if activate_local_import is False on module_config

    target_url = getattr(url, module_name)
    
    return create_class_from_url(target_url, submodule_names, activate_local_import)

