
from .remote import create_class_from_url, create_class_from_name

REMOTE_MODULES = ['http://localhost:5040/', # module_config API
                  'module_b']               # module_b API


for url in REMOTE_MODULES:

    # TODO: We might improve URL validating
    if 'http' in url:
        remote_module = create_class_from_url(url, submodule_names=None, activate_local_import=True)
    else:
        remote_module = create_class_from_name(url, submodule_names=None, activate_local_import=True)

    print('*' * 50)
    print(' Connected to ' + remote_module._module_name)
    print('*' * 50)

    locals()[remote_module._module_name] = remote_module

    del remote_module
