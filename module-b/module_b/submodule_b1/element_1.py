

DEFAULT_NAME = 'Toto'

class People:

    def __init__(self, name=DEFAULT_NAME, age=10, **kwargs):
        self.name = name
        self.age = age
    
    def get_my_name(self):
        return self.name

def tooptoop(x):
    return x